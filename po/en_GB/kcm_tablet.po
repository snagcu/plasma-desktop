# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-10 00:39+0000\n"
"PO-Revision-Date: 2022-12-31 17:00+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Primary (default)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Portrait"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Landscape"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Inverted Portrait"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Inverted Landscape"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Follow the active screen"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "Fit to Output"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Fit Output in tablet"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Custom size"

#: ui/main.qml:28
#, fuzzy, kde-format
#| msgid "No drawing tablets found."
msgid "No drawing tablets found"
msgstr "No drawing tablets found."

#: ui/main.qml:29
#, fuzzy, kde-format
#| msgid "Configure drawing tablets"
msgid "Connect a drawing tablet"
msgstr "Configure drawing tablets"

#: ui/main.qml:41
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Device:"

#: ui/main.qml:79
#, kde-format
msgid "Target display:"
msgstr "Target display:"

#: ui/main.qml:98
#, kde-format
msgid "Orientation:"
msgstr "Orientation:"

#: ui/main.qml:110
#, fuzzy, kde-format
#| msgid "Left-handed mode:"
msgid "Left handed mode:"
msgstr "Left-handed mode:"

#: ui/main.qml:120
#, kde-format
msgid "Area:"
msgstr "Area:"

#: ui/main.qml:208
#, kde-format
msgid "Resize the tablet area"
msgstr "Resize the tablet area"

#: ui/main.qml:232
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Lock aspect ratio"

#: ui/main.qml:240
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: ui/main.qml:249
#, fuzzy, kde-format
#| msgid "Button %1:"
msgid "Pen button 1:"
msgstr "Button %1:"

#: ui/main.qml:250
#, fuzzy, kde-format
#| msgid "Tool Button 2"
msgid "Pen button 2:"
msgstr "Tool Button 2"

#: ui/main.qml:251
#, fuzzy, kde-format
#| msgid "Tool Button 3"
msgid "Pen button 3:"
msgstr "Tool Button 3"

#: ui/main.qml:295
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Pad:"

#: ui/main.qml:306
#, kde-format
msgid "None"
msgstr "None"

#: ui/main.qml:328
#, fuzzy, kde-format
#| msgid "Button %1:"
msgid "Pad button %1:"
msgstr "Button %1:"

#~ msgid "Tool Button 1"
#~ msgstr "Tool Button 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "steve.allewell@gmail.com"

#~ msgid "Tablet"
#~ msgstr "Tablet"
